FROM dariksde/ubuntu-baseimage:20.04
MAINTAINER Daniel Rippen <rippendaniel@gmail.com>
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get install -y apache2 software-properties-common && \
    export LANG=C.UTF-8 && \
    add-apt-repository ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y \
      php7.4 \
      php7.4-cli \
      php7.4-fpm \
      libapache2-mod-php7.4 \
      libapache2-mod-rpaf \
      php7.4-gd \
      php7.4-intl \
      php7.4-json \
      php7.4-mbstring \
      php7.4-mysql \
      php7.4-intl \
      php7.4-sqlite3 \
      php7.4-xml \
      php7.4-zip \
      php7.4-curl \
      php7.4-opcache \
      php7.4-readline \
      php7.4-soap \
      php7.4-bcmath \
      php-imagick \
      nano \
      sendmail \
      sendmail-bin \
      mailutils && \
    apt-get clean && \
    mkdir -p /run/php && \
    rm -rf /etc/apache2/sites-available/* && \
    rm -rf /etc/apache2/sites-enabled/* && \
    mkdir -p /data/www && \
    ln -snf /data/www /var/www/https && \
    rm -rf /var/lib/apt/lists/* && \
    a2dismod php7.4 mpm_prefork ssl && \
    a2enmod actions alias proxy_fcgi mpm_event setenvif dav dav_fs rpaf rewrite && \
    a2enconf php7.4-fpm

COPY ./*.sh /usr/local/bin/
COPY ./php_mail.ini /usr/local/etc/php/conf.d/mail.ini
RUN chmod a+x /usr/local/bin/*.sh

ADD ports.conf /etc/apache2/ports.conf
ADD start.sh /start.sh

RUN sed -i -e "s/^upload_max_filesize\s*=\s*2M/upload_max_filesize = 200M/" /etc/php/7.4/fpm/php.ini && \
sed -i -e "s/^post_max_size\s*=\s*8M/post_max_size = 200M/" /etc/php/7.4/fpm/php.ini && \
sed -i -e "s/^output_buffering\s*=\s*4096/output_buffering = 0/" /etc/php/7.4/fpm/php.ini && \
sed -i -e "s/^max_execution_time\s*=\s*30/max_execution_time = 5000/" /etc/php/7.4/fpm/php.ini && \
sed -i -e "s/^max_input_time\s*=\s*60/max_input_time = 2000/" /etc/php/7.4/fpm/php.ini && \
sed -i -e "s/^memory_limit\s*=\s*128M/memory_limit = 512M/" /etc/php/7.4/fpm/php.ini && \
echo "max_input_vars = 5000" >> /etc/php/7.4/fpm/php.ini && \
echo "suhosin.get.max_vars = 5000" >> /etc/php/7.4/fpm/php.ini && \
echo "suhosin.post.max_vars = 5000" >> /etc/php/7.4/fpm/php.ini && \
echo "suhosin.request.max_vars = 5000" >> /etc/php/7.4/fpm/php.ini && \
sed -i -e "s/^pm.max_children\s*=\s*5/pm.max_children = 50/" /etc/php/7.4/fpm/pool.d/www.conf && \
sed -i -e "s/^pm.start_servers\s*=\s*2/pm.start_servers = 12/" /etc/php/7.4/fpm/pool.d/www.conf && \
sed -i -e "s/^pm.min_spare_servers\s*=\s*1/pm.min_spare_servers = 6/" /etc/php/7.4/fpm/pool.d/www.conf && \
sed -i -e "s/^pm.max_spare_servers\s*=\s*3/pm.max_spare_servers = 35/" /etc/php/7.4/fpm/pool.d/www.conf && \
echo 'catch_workers_output = yes' >> /etc/php/7.4/fpm/pool.d/www.conf && \
echo 'php_admin_value[error_log] = /var/log/php7.4-fpm.log' >> /etc/php/7.4/fpm/pool.d/www.conf && \
echo 'php_admin_flag[log_errors] = on' >> /etc/php/7.4/fpm/pool.d/www.conf

CMD ["/bin/bash", "/start.sh"]
