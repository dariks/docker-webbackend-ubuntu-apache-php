#!/bin/bash

# Start sendmail
#service sendmail start

# Setup www directory for Apache
ln -snf /data/www /var/www/https

# Start PHP-FPM
/usr/sbin/php-fpm7.4

# Load Apache2 variables and start Apache2
/bin/bash -c "source /etc/apache2/envvars && /usr/sbin/apache2 -D FOREGROUND"
